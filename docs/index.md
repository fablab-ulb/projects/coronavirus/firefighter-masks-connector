# COVID-19 :  Adapting firefighter masks into protective masks

![](images/mask6.png)

This Project is born from a collaboration between the [Brussels Firefighters](https://pompiers.brussels), the [Hospital CHU Saint-Pierre in Brussels](https://www.stpierre-bru.be/) represented by Dr. Marc Decroly, and the [Fablab ULB](http://fablab-ulb.be/) (Université Libre de Bruxelles).

The objective of this project is to provide an emergency solution for adapting firefighter masks into reusable protective masks for firefighters and caregivers.
We developed a very simple connector 22F, allowing to connect standards firefighter masks to [HMEF 1000 filters](https://www.google.com/search?q=Hmef+filter+1000&client=firefox-b-d&tbm=isch&source=iu&ictx=1&fir=jRF9a0CrDni_qM%253A%252C2A-Md_CZ06V2RM%252C_&vet=1&usg=AI4_-kTq_4gRa907_uNToKYTIawU3-XUtQ&sa=X&ved=2ahUKEwiw5cmki8roAhWLFcAKHdNrAKcQ9QEwAHoECA0QFg#imgrc=jRF9a0CrDni_qM) (filtering at 99% viruses, bacteria, fungi).

The files to reproduce the connector can be [downloaded here](#download-files), and are shared under a [Creative Commons BY-NC-SA 3.0 license](https://creativecommons.org/licenses/by-nc-sa/3.0/).

The connectors have been developed and tested in collaboration with the Hospital CHU Saint-Pierre in Brussels, which validated the adapted mask design.
**Neither the masks nor the connectors have been certified and their use is subject to a situation of mandatory need**. In particular, the sealing properties of the typical materials used for 3D printing (PLA, PETG) are limited. However, it was possible to measure a negative pressure of at least 75 mbar in the mask during inhalation when wearing it with the filter and connector.

## Connector

The connector (in blue) is made of a simple piece that can be 3D printed and an O-ring seal (*Draeger* - *R18352* O-ring (LDV) or similar).

![](images/render.jpg)
![](images/piece.png)

## Assembly

The filter and connector can be assembled to the mask as standard connectors, as shown on the picture below.

![](images/assembly.png)
![](images/mask.png)

## Fabrication

The connector has been validated using 3D printing. Note that printing technologies are different and printers can have different results. As a guidance, here are the recommended settings:

* PLA filament 1.75 mm
* Nozzle temperature: 205 – 215 ° C
* Built plate temperature: 35 - 60 ° C
* Layer thickness: 0.2 - 0.3 mm
* No Supports nor brim/raft.
* Orientation: see picture.

Note that it is possible to observe imperfections in the lower groove. However, this has no influence on the connector fitting.


![](images/print.jpg)

Other manufacturing methods can be considered depending on the volume to be produced and your facilities.  Note also that the sealing properties of the typical materials used for 3D printing (PLA, PETG) are limited.

## Cleaning, disinfection and reusability

A cleaning procedure has been developed by **Dr. Michèle Gérard** of the Hospital CHU Saint-Pierre in Brussels for reusing the mask and connector.

* **Materials** : The walls of the mask are made of EPDM. The glass is made of POLY ACETATE, the protection valve is made of SILICONE, the connector is made of Z-PETG or PLA. These materials do not tolerate heat well, except for the mask, so they must be disinfected and cold washed.
They are resistant to conventional disinfectants (chlorine or quaternary ammoniums). They are resistant to alcohol. Be careful not to use acid (silicones) or acetone or turpentine or White Spirit or cellulosic thinner or heat because the parts deform.
* **Cleaning** : Water and soap
* **Disinfection** : Quaternary Ammonium or Chlorinated solution at 1000ppm (i.e. 50 ml of 2.6% bleach in 1 litre of water). Soak for 15 minutes and rinse with clear water. Allow to dry well.
* **Filter capsule** : must be changed after 30 hours of actual use.
Simply note on the capsule with a marker every hour of actual use.
* **Storage** : not resting on the glass but mask deposited without load on the latter so as not to deform it.

# Download files

> **[Download the stl file here](files/firefighter-mask-connector.STL)**

# Other projects

Several solutions for Protective Face Masks that are also developing and producing in Fablab ULB with the help of many collaborators. [See this website for more information](https://fablab-ulb.gitlab.io/projects/coronavirus/protective-face-shields/index-en/).


# License and credits

This work is shared under a [Creative Commons BY-NC-SA 3.0 license](https://creativecommons.org/licenses/by-nc-sa/3.0/).

This Project is born from a collaboration between the [Brussels Firefighters](https://pompiers.brussels), the [Hospital CHU Saint-Pierre in Brussels](https://www.stpierre-bru.be/) represented by Dr. Marc Decroly, and the [Fablab ULB](http://fablab-ulb.be/) (Université Libre de Bruxelles) represented by Gilles Decroly. Thanks to Loïc Blanc and all the team for your contribution and your pictures.



![](images/logos.png)
