Here is a solution on filter connector for adapting firefighter masks into protective masks that we are developing in Fablab ULB in collaboration with the CHU Saint-Pierre de Bruxelles and the Brussels Firefighters.

See [this website](https://fablab-ulb.gitlab.io/projects/coronavirus/firefighter-masks-connector/) for more information.
